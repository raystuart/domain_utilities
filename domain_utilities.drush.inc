<?php

/**
 * @file
 * Drush commands for Domain Access Utilities.
 */

/**
 * Implements hook_drush_command().
 */
function domain_utilities_drush_command() {
  $items = [];

  $items['domain-purge'] = array(
    'callback' => 'drush_domain_utilities_purge',
    'description' => 'Purge a domain from the site. Deletes all content assigned to the domain.',
    'drupal dependencies' => array('domain'),
    'examples' => array(
      'drush domain-purge example.com',
      'drush domain-purge 1',
    ),
    'arguments' => array(
      'domain' => 'The domain to purge (e.g. example.com or the domain_id).',
    ),
  );

  return $items;
}

/**
 * Converts a domain string or domain_id to a $domain array.
 *
 * On failure, throws a drush error.
 */
function drush_domain_utilities_get_from_argument($argument) {
  $domain = domain_lookup($argument);
  if ($domain == -1) {
    $domain = domain_lookup(NULL, $argument, TRUE);
  }
  if ($domain == -1) {
    // drush_log(dt('Domain record not found.'), 'warning');
    return FALSE;
  }
  return $domain;
}

/**
 * Command callback. Purge content and delete a domain record.
 */
function drush_domain_utilities_purge($argument = NULL) {
  if ($argument === NULL) {
    drush_log(dt('You must specify a domain to purge.'), 'warning');
    return;
  }
  // Resolve the domain.
  if (!$domain = drush_domain_utilities_get_from_argument($argument)) {
    drush_log(dt('Domain not found: @arg', ['@arg' => $argument]), 'warning');
    return;
  }

  // Do not purge the primary domain.
  if (!empty($domain['is_default'])) {
    drush_log(dt('The primary domain may not be purged.'), 'warning');
    return;
  }

  // Only purge disabled domains.
  if (isset($domain['valid']) && $domain['valid'] !== '0') {
    drush_log(dt('Domain must be disabled before it can be purged.'), 'warning');
    return;
  }

  // Delete all the content for a domain.
  domain_utilities_purge($domain, []);

  drush_log(dt('Domain purged: @domain (@name:@id)', ['@domain' => $domain['subdomain'], '@name' => $domain['machine_name'], '@id' => $domain['domain_id']]), 'ok');
}
